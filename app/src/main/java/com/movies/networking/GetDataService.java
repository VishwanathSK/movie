package com.movies.networking;

import com.movies.model.PopularMovies;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GetDataService {

    @GET("movie/popular")
    Call<PopularMovies> getAllPopularMovies(
            @Query("api_key") String key,
            @Query("page") int page
    );

    @GET("movie/top_rated")
    Call<PopularMovies> getAllTopratedMovies(
            @Query("api_key") String key,
            @Query("page") int page
    );
}
