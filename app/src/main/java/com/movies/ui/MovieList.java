package com.movies.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.movies.R;
import com.movies.model.PopularMovies;
import com.movies.model.Result;
import com.movies.networking.GetDataService;
import com.movies.networking.RetrofitClientInstance;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieList extends AppCompatActivity {

   private String api_key = "f844df643b1485cb6ca5cc9ce562eba1";
    ListAdapter adapter;
    int popularpage=8,topratedpage=8;
    GetDataService movieApiService;
    Call<PopularMovies> call;
    List<Result> populardata = new ArrayList<>();
    List<Result> toprateddata = new ArrayList<>();
    ExtendedFloatingActionButton popular,toprated;
    boolean pop=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        popular = findViewById(R.id.popular);
        toprated = findViewById(R.id.toprated);
        popular.setOnClickListener(new ClickClass());
        toprated.setOnClickListener(new ClickClass());

        movieApiService = RetrofitClientInstance
                .getRetrofitInstance(RetrofitClientInstance.BASE_URL)
                .create(GetDataService.class);

        RecyclerView newuserrecycle = findViewById(R.id.recycle);
        newuserrecycle.setHasFixedSize(true);
        final RecyclerView.LayoutManager layoutManager = new GridLayoutManager(MovieList.this,2);
        newuserrecycle.setLayoutManager(layoutManager);
         adapter = new ListAdapter(MovieList.this);
        newuserrecycle.setAdapter(adapter);

        defaultList(R.color.colorPrimaryDark, R.color.colorPrimary, Color.WHITE, Color.BLACK, true, popularpage);


        newuserrecycle.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.d("list","newstate="+newState);
                if (!recyclerView.canScrollVertically(1) && newState==RecyclerView.SCROLL_STATE_IDLE) {
                    Log.d("-----","end");
                    if(pop){
                        popularpage++;
                        callApi(popularpage,pop);
                    }else {
                        topratedpage++;
                        callApi(topratedpage,pop);
                    }
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                Log.d("list","X="+dx+"  Y="+dy);
            }
        });

    }

    private void callApi(int page, final boolean mode) {
        if(mode) call = movieApiService.getAllPopularMovies(api_key,page);
        else call = movieApiService.getAllTopratedMovies(api_key,page);

        call.enqueue(new Callback<PopularMovies>() {
            @Override
            public void onResponse(Call<PopularMovies> call, Response<PopularMovies> response) {
                if(response!=null && response.body()!=null && response.isSuccessful()){
                    Log.d(MovieList.class.getName(),"success"+response.body().getResults().get(0).getPosterPath());
                    if(mode){
                        populardata.addAll(response.body().getResults());
                        adapter.setData(populardata);
                    }else {
                        toprateddata.addAll(response.body().getResults());
                        adapter.setData(toprateddata);
                    }

                }
            }

            @Override
            public void onFailure(Call<PopularMovies> call, Throwable t) {
                Log.d(MovieList.class.getName(),t.getMessage());
            }
        });
    }

    private class ClickClass implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.popular:
                    defaultList(R.color.colorPrimaryDark, R.color.colorPrimary, Color.WHITE, Color.BLACK, true, popularpage);
                    break;
                case R.id.toprated:
                    defaultList(R.color.colorPrimary, R.color.colorPrimaryDark, Color.BLACK, Color.WHITE, false, topratedpage);
                    break;
            }
        }
    }

    private void defaultList(int p, int p2, int s, int s2, boolean b, int popularpage) {
        popular.setBackgroundColor(getApplicationContext().getColor(p));
        toprated.setBackgroundColor(getApplicationContext().getColor(p2));
        popular.setTextColor(s);
        toprated.setTextColor(s2);
        pop = b;
        callApi(popularpage, pop);
    }
}
