package com.movies.ui;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.movies.R;
import com.movies.model.Result;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.MyViewHolder> {

    private Context context;
    private List<Result> data;

    ListAdapter(Context context) {
        this.context = context;
    }

    void setData(List<Result> data){
        Log.d("adapter",data.get(0).getPosterPath());
        this.data = data;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layoutlist_adapter, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        String IMAGE_URL = "https://image.tmdb.org/t/p/w500";
        Picasso.with(context)
                .load(IMAGE_URL +data.get(position).getPosterPath())
                .resize(holder.image.getWidth(), 800)
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        if(data!=null){
            return data.size();
        }
        return 0;
    }

     class MyViewHolder extends RecyclerView.ViewHolder{
         ImageView image;
        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.imageview);
        }
    }
}
